X-Tech Quickbooks OAuth v1.0 Service.
===============================

*Introduction*
-------------

The premise of this connector is to provide a PSR-0 Autoload standard library for conecting to the quickbooks online api.
The existing API's are complicated, large and do not use PSR-0 Autoloading standards.  For that reason, this library extends an already existing psr oauth library, lusitanian/oauth.
This library adds a new service QuickbooksOnline.

Examples
-----------

Check the examples directory for example usage.