<?php

/**
 * @abstract This file will go through the entire oauth process.  In the end it will call
 */
//bootstrap the file
	require_once "../bootstrap.php";

	//include the namespaces required
	use OAuth\OAuth1\Service\QuickbooksOnline;
	use OAuth\Common\Storage\SymfonySession;
	use OAuth\Common\Consumer\Credentials;

	$storage = new SymfonySession();

	//build credentials
	$credentials = new Credentials(
		$serviceCredentials['quickbooksOnline']['key'],
		$serviceCredentials['quickbooksOnline']['secret'],
		$currentUri->getAbsoluteUri()
	);

	$authRedirect = false;

	//instantiate the quickbooks service through the factory
	$qbService = $serviceFactory->createService('QuickbooksOnline',$credentials,$storage);

	//Check if we are ready to fetch the access token
	if(array_key_exists('oauth_verifier',$_GET))
	{
		$token = $storage->retrieveAccessToken($qbService->service());

		//build get the access token
		$qbService->requestAccess(
			$_GET['realmId'],
			$_GET['oauth_token'],
			$_GET['oauth_verifier'],
			$token->getRequestTokenSecret()
		);

		//set authRedirect to true
		$authRedirect = true;

	}else if(array_key_exists('requestToken',$_GET))
	{ //See if we have the request token

		//Get the request token
		$requestToken = $qbService->requestRequestToken();

		//forward the user to the connect Begin
		define('OAUTH_AUTHORISE_URL', 'https://appcenter.intuit.com/Connect/Begin');
		//redirect with the request token.
		header('Location: '. OAUTH_AUTHORISE_URL .'?oauth_token='.$requestToken->getRequestToken());
		exit();
	}
?>
<!doctype html>
<html>
	<head>
		<script src="https://js.appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js" type="text/javascript"></script>
		<?php if(!$storage->hasAuthorizationState($qbService->service())):?>
		<script>
			intuit.ipp.anywhere.setup({
          		menuProxy: '',
          		grantUrl: 'https://example.com/requestToken.php'});
     	</script>
     	<?php elseif($authRedirect) :?>
     		<script>
     			window.opener.location.reload(false);
				window.close();
     		</script>
     	<?php endif?>
	</head>
	<body>
		<?php if(!$storage->hasAccessToken($qbService->service())):?>
			<ipp:connectToIntuit></ipp:connectToIntuit>
		<?php else: ?>
			<?php

				$token= $storage->retrieveAccessToken($qbService->service());
				$query = "SELECT * FROM Customer";

				$result = json_decode($qbService->request('/company/'.$qbService->getCompanyId().'/query?query='.urlencode($query)));

				echo "<pre>";
				print_r($result);
				echo "</pre>";
			?>
		<?php endif ?>
	</body>
</html>