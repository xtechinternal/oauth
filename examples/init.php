<?php

/**
 * This file sets up the information needed to test the examples in different environments.
*
* PHP version 5.4
*
* @author     David Desberg <david@daviddesberg.com>
* @author     Pieter Hordijk <info@pieterhordijk.com>
* @author 	  Andrew Chinn <andrew.chinn@xtechhq.com>
* @copyright  Copyright (c) 2012 The authors
* @license    http://www.opensource.org/licenses/mit-license.html  MIT License
*/

/**
 * @var array A list of all the credentials to be used by the different services in the examples
*/
$serviceCredentials = array(
		'quickbooksOnline'=> array(
			'key'=>'qyprd9DTFdNi7fJjLOysuGUomFDy71',
			'secret'=>'36cdC7j2GJlY6GRY2Oc16V6MmDD6xqmA9Jg1WgdB'
		)
);

/** @var $serviceFactory \OAuth\ServiceFactory An OAuth service factory. */
$serviceFactory = new \OAuth\ServiceFactory();