<?php
	/**
	 * @author 	  Andrew Chinn <andrew.chinn@xtechhq.com>
	 * @author     David Desberg <david@daviddesberg.com>
	 * @author     Pieter Hordijk <info@pieterhordijk.com>
	 * @copyright  Copyright (c) 2012 The authors
	 * @license    http://www.opensource.org/licenses/mit-license.html  MIT License
	**/
	namespace OAuth\OAuth1\Service;

	use OAuth\OAuth1\Signature\SignatureInterface;
	use OAuth\OAuth1\Token\StdOAuth1Token;
	use OAuth\Common\Http\Exception\TokenResponseException;
	use OAuth\Common\Http\Uri\Uri;
	use OAuth\Common\Consumer\CredentialsInterface;
	use OAuth\Common\Http\Uri\UriInterface;
	use OAuth\Common\Storage\TokenStorageInterface;
	use OAuth\Common\Http\Client\ClientInterface;

	class QuickbooksOnline extends AbstractService{

		//constant for consistent access to authorize state key
		const companyIdStateString = 'companyId';


		/**
		 *
		 * @param CredentialsInterface $credentials
		 * @param ClientInterface $httpClient
		 * @param TokenStorageInterface $storage
		 * @param SignatureInterface $signature
		 * @param UriInterface $baseApiUri
		 */
		public function __construct(
	        CredentialsInterface $credentials,
	        ClientInterface $httpClient,
	        TokenStorageInterface $storage,
	        SignatureInterface $signature,
	        UriInterface $baseApiUri = null
		) {
	        parent::__construct($credentials, $httpClient, $storage, $signature, $baseApiUri);

	        if (null === $baseApiUri) {
	            $this->baseApiUri = new Uri('https://qb.sbfinance.intuit.com/v3/');
	        }
		}

		/**
		 * @abstract This method returns the reconnect uri.
		 * @return \OAuth\Common\Http\Uri\Uri
		 */
		public function getRecconectTokenEndpoint()
		{
			return new Uri('https://appcenter.intuit.com/api/v1/connection/reconnect');
		}

		/**
		 * (non-PHPdoc)
		 * @see \OAuth\OAuth1\Service\ServiceInterface::getRequestTokenEndpoint()
		 */
	    public function getRequestTokenEndpoint()
	    {
	        return new Uri('https://oauth.intuit.com/oauth/v1/get_request_token');
	    }

        /**
         * (non-PHPdoc)
         * @see \OAuth\Common\Service\ServiceInterface::getAuthorizationEndpoint()
         */
	    public function getAuthorizationEndpoint()
	    {
	        return new Uri('https://appcenter.intuit.com/Connect/Begin');
	    }

	    /**
	     * (non-PHPdoc)
	     * @see \OAuth\Common\Service\ServiceInterface::getAccessTokenEndpoint()
	     */
	    public function getAccessTokenEndpoint()
	    {
	        return new Uri('https://oauth.intuit.com/oauth/v1/get_access_token');
	    }

	    /**
	     * This method returns the company id that is stored in the token authorization state
	     * @return string|NULL
	     */
	    public function getCompanyId()
	    {
	    	$state = $this->storage->retrieveAuthorizationState($this->service());
	    	if(array_key_exists($this::companyIdStateString,$state))
	    	{
	    		return $state[$this::companyIdStateString];
	    	}
	    	return null;
	    }

	    protected function parseRequestTokenResponse($responseBody)
	    {
	        parse_str($responseBody, $data);

	        if (null === $data || !is_array($data)) {
	            throw new TokenResponseException('Unable to parse response.');
	        } elseif (!isset($data['oauth_callback_confirmed']) || $data['oauth_callback_confirmed'] !== 'true') {
	            throw new TokenResponseException('Error in retrieving token.');
	        }

	        return $this->parseAccessTokenResponse($responseBody);
	    }


	    protected function parseAccessTokenResponse($responseBody)
	    {
	        parse_str($responseBody, $data);

	        if (null === $data || !is_array($data)) {
	            throw new TokenResponseException('Unable to parse response.');
	        } elseif (isset($data['error'])) {
	            throw new TokenResponseException('Error in retrieving token: "' . $data['error'] . '"');
	        }

	        $token = new StdOAuth1Token();

	        $token->setRequestToken($data['oauth_token']);
	        $token->setRequestTokenSecret($data['oauth_token_secret']);
	        $token->setAccessToken($data['oauth_token']);
	        $token->setAccessTokenSecret($data['oauth_token_secret']);

	        $token->setEndOfLife(StdOAuth1Token::EOL_NEVER_EXPIRES);
	        unset($data['oauth_token'], $data['oauth_token_secret']);
	        $token->setExtraParams($data);

	        return $token;
	    }

		public function requestAccess($realmId,$token,$verifier,$tokenSecret = null)
		{
			//Set the company Id in the Authorization State
			$this->storage->storeAuthorizationState($this->service(), array($this::companyIdStateString=>$realmId));

			//Fetch the access token
			return $this->requestAccessToken($token, $verifier,$tokenSecret);

		}
		
	    public function requestAccessToken($token, $verifier, $tokenSecret = null)
	    {
	    	if (is_null($tokenSecret)) {
	    		$storedRequestToken = $this->storage->retrieveAccessToken($this->service());
	    		$tokenSecret = $storedRequestToken->getRequestTokenSecret();
	    	}
	    	$this->signature->setTokenSecret($tokenSecret);


	    	$bodyParams = array(
	    			'oauth_verifier' => $verifier,
	    			'oauth_callback' => 'oob'
	    	);

			$header[] = 'ACCEPT: text/json';

	    	$authorizationHeader = array(
	    			'Authorization' => $this->buildAuthorizationHeaderForAPIRequest(
	    					'POST',
	    					$this->getAccessTokenEndpoint(),
	    					$this->storage->retrieveAccessToken($this->service()),
	    					$bodyParams
	    			)
	    	);
	    	$header[] = 'Authorization: '.$authorizationHeader['Authorization'];



	    	$ch = curl_init($this->getAccessTokenEndpoint()->getAbsoluteUri());


	    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
	    	curl_setopt($ch, CURLOPT_POST,1);
	    	curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyParams);
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setOpt($ch, CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setOpt($ch, CURLINFO_HEADER_OUT,1);


	    	$responseBody = curl_exec($ch);

	    	$token = $this->parseAccessTokenResponse($responseBody);
	    	$this->storage->storeAccessToken($this->service(), $token);

	    	//Store the token in the authorization state
	    	$this->storage->storeAuthorizationState($this->service(), $token);
	    	return $token;
	    }

	    public function requestTokenRefresh()
	    {
	    	$uri = $this->getRecconectTokenEndpoint();
	    	$token = $this->storage->retrieveAccessToken($this->service());
	    	$extraHeaders = array_merge($this->getExtraApiHeaders(), $extraHeaders);
	    	$authorizationHeader = array(
	    			'Authorization' => $this->buildAuthorizationHeaderForAPIRequest('GET', $uri, $token, array())
	    	);
	    	$headers = array_merge($authorizationHeader, $extraHeaders);

	    	return $this->httpClient->retrieveResponse($uri, array(), $headers, 'GET');
	    }

	    public function getExtraOAuthHeaders()
	    {
	    	return array();
	    }
	    public function getExtraApiHeaders()
	    {
	    	return array('Accept'=>'application/json');
	    }
	}